drop table if exists posts;
drop table if exists comments;

create table posts (
    id integer not null primary key autoincrement,
    icon varchar(80),
    name varchar(80) not null,
    title varchar(80) not null,
    body text default ''
); 

insert into posts values (null, "images/default_icon.png",  "John", "Very first post", "This the first post on the site");
insert into posts values (null, "images/icon1.png",  "Rose", "Hello", "Just wanted to say hi");
insert into posts values (null, "images/icon2.jpg",  "Jade", "Good Dog", "Best Friend");
insert into posts values (null, "images/icon3.png",  "Dave", "Something", "Didn't know what to write");

create table comments (
    id integer not null primary key autoincrement,
    name varchar(80) not null,
    body text default '',
    postid integer not null,
    FOREIGN KEY(postid) REFERENCES posts(id)
    ON DELETE CASCADE
);

insert into comments values (null, "Dirk", "Awesome, you got the first post.",1);
insert into comments values (null, "Jake", "Nice.",1);
insert into comments values (null, "Jane", "Hello",2);
insert into comments values (null, "Roxy", "Didn't know what to comment",3);