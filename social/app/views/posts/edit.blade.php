@extends('layouts.master')

@section('header')

<form method="post" action="{{{ url("edit_post_action/$post->id") }}}">
<p style="color:White; font-size:1.75em">Edit Post:</p>
    Name: <input type='text' name='name'><br>
    Title: <input type='text' name='title'><br>
    Message: <br>
    <textarea rows='' cols='' width:"100%" name='message' style="color:black">Enter Your Comment</textarea><br>
    <input type="submit" value="Post">
</form>

<form action="{{{ url("posts") }}}">
    <input type="submit" value="Cancel">
</form>

@stop