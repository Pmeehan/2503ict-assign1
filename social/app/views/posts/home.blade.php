@extends('layouts.master')

@section('header')
<form method="post" action="add_post_action">
<p style="color:White; font-size:1.75em">Enter a post:</p>
<span style="color:White">
    Name: <input type='text' name='name'><br>
    Title: <input type='text' name='title'><br>
    Message: <br>
    <textarea rows='' cols='' width:"100%" name='message' style="color:black">Enter Your Post</textarea><br>
    <input type="submit" value="Post">
</span>
</form>
<hr style="border-color:LightGray">

@stop

@section('content')

<p style="color:White; font-size:1.75em">All Posts</p><br>

@if (count($posts) == 0)

<p>No posts to display</p>

@else

@foreach($posts as $post)
    <?php
        $count = 0;
        foreach($comments as $comment) {
            if($comment->postid == $post->id) {
                $count = $comment->comcount;
            }
        }
    ?>
    <div class='post'>
        <table style="width:100%;">
            <tr>
                <td rowspan="3" style="width:100px"><img class='photo' src={{{ $post->icon }}} alt='example image'></td>
                <td style="text-align:left"><span class="posttitle">{{{ $post->title }}} by {{{ $post->name }}}</span></td>
            </tr>
            <tr>
                <td style="text-align:left"><span style="color:LightGray">{{{ $post->body }}}</span></td>
            </tr>
            <tr>
                <td style="text-align:right">
                    <span style="font-size:0.75em; color:#1F1F3C; vertical-align:bottom">
                    <a href="{{{ url("comments/$post->id") }}}">Comments({{{ $count }}})</a> || 
                    <a href="{{{ url("edit/$post->id") }}}">Edit</a> || 
                    <a href="{{{ url("delete_post_action/$post->id") }}}">Delete</a>
                    </span>
                    </td>
            </tr>
        </table>
    </div>
@endforeach    

@endif

@stop