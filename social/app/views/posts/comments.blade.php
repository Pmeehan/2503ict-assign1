@extends('layouts.master')

@section('header')
    <div class='post'>
        <table style="width:100%;">
            <tr>
                <td rowspan="3" style="width:100px"><img class='photo' src={{{ $post->icon }}} alt='example image'></td>
                <td style="text-align:left"><span class="posttitle">{{{ $post->title }}} by {{{ $post->name }}}</span></td>
            </tr>
            <tr>
                <td style="text-align:left"><span style="color:LightGray">{{{ $post->body }}}</span></td>
            </tr>
            <tr>
                <td style="text-align:right">
                    <span style="font-size:0.75em; color:#1F1F3C; vertical-align:bottom">
                    <a href="{{{ url("edit/$post->id") }}}">Edit</a> || 
                    <a href="{{{ url("delete_post_action/$post->id") }}}">Delete</a>
                    </span>
                    </td>
            </tr>
        </table>
    </div>
@stop

@section('content')

<p style="color:White; font-size:1.75em">All Comments</p><br>

@if (count($comments) == 0)

<p>No comments to display</p>

@else

@foreach($comments as $comment)
    <div class='comment'>
    {{{ $comment->name }}} Says<br>
    {{{ $comment->body }}}<br>
    <a href="{{{ url("delete_comment_action/$comment->id/$post->id") }}}">(delete this comment)</a><br>
    </div>
@endforeach    

@endif
<hr>
<form method="post" action="{{{ url("add_comment_action/$post->id") }}}">
<p style="color:White; font-size:1.75em">Enter a comment:</p>
    Name: <input type='text' name='name'><br>
    Message: <br>
    <textarea rows='' cols='' width:"100%" name='message' style="color:black">Enter Your Comment</textarea><br>
    <input type="submit" value="Post">
</form>

@stop