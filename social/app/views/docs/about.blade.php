@extends('layouts.master')

@section('header')

About this website:
<br>
This website was created by Patrick Meehan as part for 2503ICT assignment 1.
<br>
<br>
Functionality:
<br>
This website meets every requirement specified in the assignment task sheet, however it does not perform any extra functions beyond this. Refer to the assignment task sheet for more specific details on what functionality was required
<br>
<br>
Interesting Approaches:
<br>
The approach to the development of this website was fairly standard and in general it was closely modelled from the existing lab exercises. There is nothing noteworthy to comment in this regard with the exception of one issue:
for a reason I was unable to determine, the website is unable to properly fetch the URL for assets in the public/ directory outside of the websites homepage. This affects the style sheet and image URL. It appears to stem 
from an issue with laravel routing and I was unable to resolve it with blade calls such as using HTML :: style(url('css/styles.css'))



@stop