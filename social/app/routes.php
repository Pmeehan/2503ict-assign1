<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
  return Redirect::to(url("posts"));
});

Route::get('posts', function()
{
  $posts = get_posts();
  $comments = comment_table();
  
  //sort posts into reverse order
  //naively, the higher the id the more recently the post was created
  //therefore, if we sort the posts by index in descending order
  //this will put the most recently created first
  //so just reverse the results of the query (which are in ascending order)
  rsort($posts); 

	return View::make('posts.home')->withPosts($posts)->withComments($comments);
});

Route::get('comments/{id}', function ($id)
{
  $post = get_post($id);
  $comments = get_comments($id);

	return View::make('posts.comments')->withPost($post)->withComments($comments);
}); 

Route::post('add_post_action', function()
{
  $name = Input::get('name');
  $title = Input::get('title');
  $message = Input::get('message');

  $id = add_post($name, $title, $message);

  return Redirect::to(url("posts"));
});

Route::post('add_comment_action/{id}', function($id)
{
  $name = Input::get('name');
  $message = Input::get('message');

  add_comment($name, $message, $id);

  return Redirect::to(url("comments/$id"));
});

Route::get('delete_post_action/{id}', function($id)
{
  delete_post($id);
  return Redirect::to(url("posts"));
});

Route::get('delete_comment_action/{id}/{postid}', function($id, $postid)
{
  delete_comment($id);
  return Redirect::to(url("comments/$postid"));
});

Route::get('edit/{id}', function($id)
{
  $post= get_post($id);
  return View::make('posts.edit')->withPost($post);
});

Route::post('edit_post_action/{id}', function($id)
{
  $name = Input::get('name');
  $title = Input::get('title');
  $message = Input::get('message'); 
  
  update_post($name, $title, $message, $id);
  
  return Redirect::to(url("comments/$id"));
});

Route::get('about', function()
{
  return View::make('docs.about');
});

Route::get('ER', function()
{
  return View::make('docs.ER');
});

Route::get('Layout', function()
{
  return View::make('docs.Layout');
});

//gets all posts
function get_posts()
{
  $sql = "select * from posts";
  $posts = DB::select($sql);
  return $posts;
}

/* Gets post with the given id */
function get_post($id)
{
	$sql = "select * from posts where id = ?";
	$items = DB::select($sql, array($id));

	// If we get more than one post or no posts display an error
	if (count($items) != 1) 
	{
    die("Invalid query or result:\n");
  }

	// Extract the first item (which should be the only item)
  $item = $items[0];
	return $item;
}

//add a post to the database with given parameters
function add_post($name, $title, $body) 
{
  $sql = "insert into posts (icon, name, title, body) values ('images/default_icon.png', ?, ?, ?)";

  DB::insert($sql, array($name, $title, $body));
}

//add a comment to the database with given parameters
function add_comment($name, $body, $postid) 
{
  $sql = "insert into comments (name, body, postid) values (?, ?, ?)";

  DB::insert($sql, array($name, $body, $postid));
}

//update the specified post with given parameters
function update_post($name, $title, $message, $id) 
{
  $sql = "UPDATE posts SET name=?, title=?, body=? WHERE id=?";

  DB::update($sql, array($name, $title, $message, $id));
}

//delete post with given id and update database
function delete_post($id) 
{
  $sql = "DELETE FROM posts WHERE id=?";

  DB::update($sql, array($id));
}

//delete comment with given id and update database
function delete_comment($id) 
{
  $sql = "DELETE FROM comments WHERE id=?";

  DB::update($sql, array($id));
}


//get comment count for post with given id
function comment_count($id)
{
  $sql = "SELECT COUNT(postid) AS comcount FROM comments WHERE postid=?";
  
  $results = DB::select($sql, array($id));
  
  if(count($results) != 1)
  {
    return ($results[0]->comcount);
  } else {
    return 0;
  }
}

//generate table with comment count for all posts
//used to determine number of comments for each post on main page
function comment_table()
{
  $sql = "SELECT postid,COUNT(postid) AS comcount FROM comments GROUP BY postid";
  
  $results = DB::select($sql);
  
  return $results;
}

//get all comments for post with given id
function get_comments($id)
{
    $sql = "SELECT * FROM comments WHERE postid LIKE ?";
    
    $results = DB::select($sql, array($id));
    
    return $results;
}